﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CurrentPlayerUIManager : MonoBehaviour {

    public Sprite swordMaster;
    public Sprite archer;

    public Text classText;
    public Text hpText;
    public Text strengthText;
    public Text goldText;
    public Text movementText;
    public Image panel;
    public Image classIcon;

    PlayerManager currentPlayer;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        currentPlayer = GameManager.instance.currentPlayer;
        
        if(currentPlayer.team == 'R')
        {
            panel.color = Color.red;
        }
        else if(currentPlayer.team == 'B')
        {
            panel.color = Color.blue;
        }
        
        if(currentPlayer.currentPawnClass == PlayerManager.PawnClass.SwordMaster)
        {
            classText.text = "Sword Master";
            classIcon.sprite = swordMaster;
        }
        else if(currentPlayer.currentPawnClass == PlayerManager.PawnClass.Archer)
        {
            classText.text = "Archer";
            classIcon.sprite = archer;
        }

        hpText.text = currentPlayer.currentHealth.ToString();
        strengthText.text = currentPlayer.strength.ToString();
        goldText.text = currentPlayer.gold.ToString();
        movementText.text = currentPlayer.movementRange.ToString();
	}
}
