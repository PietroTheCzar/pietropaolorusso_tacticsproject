﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class TileMap : Singleton<TileMap>
{
    public GameManager myGameManager;
    public TurnManager myTurnManager;
   // ClickableTile myTiles;
    
    public GameObject currentUnit;
    public PlayerManager currentPlayer;

    public TileType[] tileTypes;

    public int[,] tiles;
    public Node[,] graph;

    public int maxMovePerTurn;
    public int combatRange;

    [HideInInspector]
    public int mapSizeX = 12;
    [HideInInspector]
    public int mapSizeZ = 12;

    int pathWeight;

    public bool pathsAvailable = false;
    public bool pathChosen = false;
    public bool combatReady = false;

    public Camera gameCamera;

    public Material grassMaterial;
    public Material sandMaterial;
    public Material rockMaterial;

    void Awake()
    {
        GenerateMapData();
        GeneratePathfindingGraph();
    }

    private void Start()
    {
        //TurnManager.instance.OnStartTurn += UpdateCurrentPlayer;
        TurnManager.instance.OnMoveTurn += GeneratePossiblePaths;
        TurnManager.instance.OnCombatTurn += GenerateCombatRange;
        GameManager.instance.OnCurrentPlayerUpdate += UpdateCurrentPlayer;
        UpdateCurrentPlayer();
        StatsManager.instance.SaveGameStats();
        //currentPlayer.map = this;
    }

    private void OnDestroy()
    {
        //TurnManager.instance.OnStartTurn -= UpdateCurrentPlayer;
        TurnManager.instance.OnMoveTurn -= GeneratePossiblePaths;
        TurnManager.instance.OnCombatTurn -= GenerateCombatRange;
        GameManager.instance.OnCurrentPlayerUpdate -= UpdateCurrentPlayer;
    }
    private void Update()
    {
        RaycastHit hit;
        Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray, out hit) && GameManager.instance.gameEnded == false)
        {
            if(hit.collider.tag == "Tile")
            {
                if (currentPlayer.isMoving == false && pathsAvailable == true && pathChosen == false)
                {
                    List<Node> maybePath = GeneratePathTo(hit.collider.transform.GetComponent<ClickableTile>().tileX, hit.collider.transform.GetComponent<ClickableTile>().tileZ);

                    if(!(currentPlayer.tileX == hit.collider.transform.GetComponent<ClickableTile>().tileX && currentPlayer.tileZ == hit.collider.transform.GetComponent<ClickableTile>().tileZ))
                    {
                        foreach (Node node in graph)
                        {
                            if (node.isAvailable == true)
                            {
                                node.tile.transform.GetComponent<MeshRenderer>().material.color = Color.yellow;
                            }
                            else
                            {
                                node.tile.transform.GetComponent<MeshRenderer>().material.color = node.baseColor;
                            }
                        }
                        if (maybePath.Count <= maxMovePerTurn && maybePath != null)
                        {
                            currentPlayer.currentPath = maybePath;

                            foreach (Node n in currentPlayer.currentPath)
                            {
                                n.tile.transform.GetComponent<MeshRenderer>().material.color = Color.red;
                            }
                        }
                    }
                }
            }
        }
    }

    public void UpdateMapColors()
    {
        foreach(Node node in graph)
        {
            if(node.isCurrentPath == true)
            {
                node.tile.transform.GetComponent<Renderer>().material.color = Color.yellow;
            }
            else
            {
                node.tile.transform.GetComponent<Renderer>().material.color = node.baseColor;
            }
        }
    }

    void GenerateMapData()
    {
        tiles = new int[mapSizeX, mapSizeZ];

        int tileID;

        for (int x = 0; x < mapSizeX; x++)
        {
            for (int z = 0; z < mapSizeZ; z++)
            {
                tileID = UnityEngine.Random.Range(0, 101);
                if (tileID <= 50)
                {
                    tiles[x, z] = 0;
                }
                else if (tileID <= 90 && tileID > 50)
                {
                    tiles[x, z] = 1;
                }
                else if (tileID > 90)
                {
                    tiles[x, z] = 2;
                }
            }
        }
    }

    public float CostToEnterTile(int sourceX, int sourceZ, int targetX, int targetZ)
    {

        TileType tt = tileTypes[tiles[targetX, targetZ]];

        float cost = tt.movementCost;

        return cost;

    }

    void GeneratePathfindingGraph()
    {
        graph = new Node[mapSizeX, mapSizeZ];

        for (int x = 0; x < mapSizeX; x++)
        {
            for (int z = 0; z < mapSizeX; z++)
            {
                graph[x, z] = new Node();
                graph[x, z].x = x;
                graph[x, z].z = z;

                TileType tt = tileTypes[tiles[x, z]];
                if (tiles[x, z] == 0)
                {
                    graph[x, z].baseColor = grassMaterial.color;
                    GameObject go = (GameObject)Instantiate(tt.tileVisualPrefab, new Vector3(x, 0, z), Quaternion.identity);
                    go.transform.GetComponent<Renderer>().material.color = graph[x, z].baseColor;
                    graph[x, z].tile = go;
                    ClickableTile ct = go.GetComponent<ClickableTile>();
                    ct.pawnPosition = new Vector3(x, go.transform.position.y + 1.4f, z);
                    graph[x, z].pathPosition = ct.pawnPosition;
                    ct.tileX = x;
                    ct.tileZ = z;
                    ct.map = this;
                    graph[x, z].baseMovementWeight = 1;
                    graph[x, z].movementWeight = graph[x, z].baseMovementWeight;
                }
                if (tiles[x, z] == 1)
                {
                    graph[x, z].baseColor = sandMaterial.color;
                    GameObject go = (GameObject)Instantiate(tt.tileVisualPrefab, new Vector3(x, 1, z), Quaternion.identity);
                    go.transform.GetComponent<Renderer>().material.color = graph[x, z].baseColor;
                    graph[x, z].tile = go;
                    ClickableTile ct = go.GetComponent<ClickableTile>();
                    ct.pawnPosition = new Vector3(x, go.transform.position.y + 1.4f, z);
                    graph[x, z].pathPosition = ct.pawnPosition;
                    ct.tileX = x;
                    ct.tileZ = z;
                    ct.map = this;
                    graph[x, z].baseMovementWeight = 2;
                    graph[x, z].movementWeight = graph[x, z].baseMovementWeight;
                }
                if (tiles[x, z] == 2)
                {
                    graph[x, z].baseColor = rockMaterial.color;
                    GameObject go = (GameObject)Instantiate(tt.tileVisualPrefab, new Vector3(x, 1, z), Quaternion.identity);
                    go.transform.GetComponent<Renderer>().material.color = graph[x, z].baseColor;
                    graph[x, z].tile = go;
                    ClickableTile ct = go.GetComponent<ClickableTile>();
                    ct.pawnPosition = new Vector3(x, go.transform.position.y + 1.4f, z);
                    graph[x, z].pathPosition = ct.pawnPosition;
                    ct.tileX = x;
                    ct.tileZ = z;
                    ct.map = this;
                    graph[x, z].baseMovementWeight = 4;
                    graph[x, z].movementWeight = graph[x, z].baseMovementWeight;
                }
            }
        }

        for (int x = 0; x < mapSizeX; x++)
        {
            for (int z = 0; z < mapSizeZ; z++)
            {
                if (x > 0)
                {
                    graph[x, z].neighbours.Add(graph[x - 1, z]);
                }
                if (x < mapSizeX - 1)
                {
                    graph[x, z].neighbours.Add(graph[x + 1, z]);
                }
                if (z > 0)
                {
                    graph[x, z].neighbours.Add(graph[x, z - 1]);
                }
                if (z < mapSizeZ - 1)
                {
                    graph[x, z].neighbours.Add(graph[x, z + 1]);
                }
            }
        }
    }

    public Vector3 TileCoordToWorldCoord(int x, int z)
    {
        if(tiles[x, z] == 0)
        {
           float y = 1;
           return new Vector3(x, y, z);
        }
        else 
        {
           float y = 2;
           return new Vector3(x, y, z);
        }
    }

    public List<Node> GeneratePathTo(int x, int z)
    {
        bool free = true;
        foreach (GameObject go in GameManager.instance.blueTeamPawns)
        {
            if (GameManager.instance.currentPlayer != go.GetComponent<PlayerManager>() && go.GetComponent<PlayerManager>().tileX == x && go.GetComponent<PlayerManager>().tileZ == z)
            {
                free = false;
                break;
            }
        }
        if (free)
        {
            foreach (GameObject go in GameManager.instance.redTeamPawns)
            {
                if (GameManager.instance.currentPlayer != go.GetComponent<PlayerManager>() && go.GetComponent<PlayerManager>().tileX == x && go.GetComponent<PlayerManager>().tileZ == z)
                {
                    free = false;
                    break;
                }
            }
        }
        if (!free)
        {
            return null;
        }

        currentPlayer.currentPath = null;
        currentPlayer.currentPositions = null;
        pathWeight = 0;

        Dictionary<Node, float> dist = new Dictionary<Node, float>();
        Dictionary<Node, Node> prev = new Dictionary<Node, Node>();
        List<Vector3> currentPathPositions = new List<Vector3>();

        List<Node> unvisited = new List<Node>();

        Node source = graph[currentPlayer.tileX, currentPlayer.tileZ];

        Node target = graph[x, z];

        dist[source] = 0;
        prev[source] = null;

        foreach (Node v in graph)
        {
            if (v != source)
            {
                dist[v] = Mathf.Infinity;
                prev[v] = null;
            }
            free = true;
            foreach(GameObject go in GameManager.instance.blueTeamPawns) {
                if(GameManager.instance.currentPlayer != go.GetComponent<PlayerManager>() && go.GetComponent<PlayerManager>().tileX == v.x && go.GetComponent<PlayerManager>().tileZ == v.z)
                {
                    free = false;
                    break;
                }
            }
            if (free)
            {
                foreach (GameObject go in GameManager.instance.redTeamPawns)
                {
                    if (GameManager.instance.currentPlayer != go.GetComponent<PlayerManager>() && go.GetComponent<PlayerManager>().tileX == v.x && go.GetComponent<PlayerManager>().tileZ == v.z)
                    {
                        free = false;
                        break;
                    }
                }
            }
            if (free)
            {
                unvisited.Add(v);
            }
        }

        while (unvisited.Count > 0)
        {
            Node u = null;

            foreach (Node possibleU in unvisited)
            {
                if (u == null || dist[possibleU] < dist[u])
                {
                    u = possibleU;
                }
            }

            if (u == target)
            {
                break;
            }

            unvisited.Remove(u);

            foreach (Node v in u.neighbours)
            {
                float alt = dist[u] + CostToEnterTile(u.x, u.z, v.x, v.z);
                if (alt < dist[v])
                {
                    dist[v] = alt;
                    prev[v] = u;
                }
            }
        }

        if (prev[target] == null)
        {
            return null;
        }

        List<Node> currentPath = new List<Node>();

        Node curr = target;

        while (curr != null)
        {
            currentPath.Add(curr);
            curr = prev[curr];
        }

        currentPath.Add(source);
        currentPath.Reverse();

        if (currentPath.Count <= maxMovePerTurn && currentPath.Count > 1)
        {

            foreach (Node node in currentPath)
            {
                currentPathPositions.Add(node.pathPosition);
            }

            foreach (Node node in currentPath)
            {
                pathWeight += node.movementWeight;
            }

            currentPlayer.currentPath = currentPath;
            currentPlayer.currentPositions = currentPathPositions;
        }

        return currentPath;
    }
    
    public void ResetBoard()
    {
        Node source = graph[currentPlayer.tileX, currentPlayer.tileZ];
        pathsAvailable = false;
        foreach (Node node in graph)
        {
            node.tile.transform.GetComponent<Renderer>().material.color = node.baseColor;
            node.tile.transform.GetComponent<ClickableTile>().inCombatRange = false;
        }
    }

    public void GeneratePossiblePaths()
    {
        Debug.Log("Paths Generated");
        ResetBoard();
        pathChosen = false;
        foreach(Node node in graph)
        {
            node.isAvailable = false;
        }
        List<List<Node>> PossiblePaths = new List<List<Node>>();
        foreach (Node n in graph)
        {
            List<Node> newPath = GeneratePathTo(n.x, n.z);
            if(newPath != null && newPath.Count <= maxMovePerTurn && newPath.Count > 1)
            {
                PossiblePaths.Add(newPath);
            }
        }
        if(PossiblePaths.Count > 0)
        {
            foreach(List<Node> CheckingPath in PossiblePaths)
            {
                foreach(Node node in CheckingPath)
                {
                    node.tile.transform.GetComponent<Renderer>().material.color = Color.yellow;
                    node.isAvailable = true;
                }
            }
        }
        pathsAvailable = true;
        
    }

    public void GenerateCombatRange()
    {
        if(currentPlayer.isMoving == false)
        {
            ResetBoard();
            combatReady = true;

            foreach (Node node in graph)
            {
                if ((((node.x <= currentPlayer.tileX + combatRange && node.x > currentPlayer.tileX) || (node.x >= currentPlayer.tileX - combatRange && node.x < currentPlayer.tileX)) && node.z == currentPlayer.tileZ) || (((node.z <= currentPlayer.tileZ + combatRange && node.z > currentPlayer.tileZ)  || (node.z >= currentPlayer.tileZ - combatRange && node.z < currentPlayer.tileZ)) && node.x == currentPlayer.tileX))
                {
                    node.tile.transform.GetComponent<Renderer>().material.color = Color.red;
                    node.attackAvailable = true;
                    node.tile.transform.GetComponent<ClickableTile>().inCombatRange = true;
                }
            }
        }
    }

    public void UpdateCurrentPlayer()
    {
        Debug.Log("Updated Current Player - Tile Map");
        currentPlayer = myGameManager.currentPlayer;
        currentUnit = myGameManager.currentPawn;
        currentPlayer.tileX = (int)currentUnit.transform.position.x;
        currentPlayer.tileZ = (int)currentUnit.transform.position.z;
        maxMovePerTurn = currentPlayer.movementRange;
        combatRange = currentPlayer.combatRange;
        TurnManager.instance.SkipToNextGamePhase();
    }
}