﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class ClickableTile : MonoBehaviour
{

    public int tileX;
    public int tileZ;

    public Vector3 pawnPosition;

    public TileMap map;
    public Node nodeGraph;

    public bool inCombatRange = false;

    Renderer tileColor;
    GameManager myGameManager;

    private void Start()
    {
        tileColor = transform.GetComponent<MeshRenderer>();
        myGameManager = map.myGameManager;
    }

    private void OnMouseUp()
    {
        if(map.currentPlayer.isMoving == false && map.pathsAvailable == true && GameManager.instance.gameEnded == false && ShopManager.instance.shopOpen == false)
        {
            Debug.Log("Click!");
            if(map.pathChosen == false)
            {
                map.pathChosen = true;
            }
            else if(map.pathChosen == true)
            {
                map.pathChosen = false;
            }
            List<Node> maybePath = map.GeneratePathTo(tileX, tileZ);
            
            foreach (Node node in map.graph)
            {
                if(node.isAvailable == true)
                {
                    node.tile.transform.GetComponent<Renderer>().material.color = Color.yellow;
                }
                else
                {
                    node.tile.transform.GetComponent<Renderer>().material.color = node.baseColor;
                }
            }

            if (maybePath.Count <= map.maxMovePerTurn && maybePath.Count > 1) {
                map.currentPlayer.currentPath = maybePath;

                foreach (Node n in map.currentPlayer.currentPath)
                {
                    n.tile.transform.GetComponent<Renderer>().material.color = Color.red;
                }
            }

            map.currentPlayer.MovePawn();
            if (map.currentPlayer.currentPawnClass == PlayerManager.PawnClass.Archer)
            {
                int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.archerMoveQuotes.Length);
                SoundManager.instance.Play(SoundManager.instance.archerMoveQuotes[randomIndex], false);
            }
            else if (map.currentPlayer.currentPawnClass == PlayerManager.PawnClass.SwordMaster)
            {
                int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.warriorMoveQuotes.Length);
                SoundManager.instance.Play(SoundManager.instance.warriorMoveQuotes[randomIndex], false);
            }

            StatsManager.instance.SaveGameStats();
        }

        if(inCombatRange == true && !(tileX == map.currentPlayer.tileX && tileZ == map.currentPlayer.tileZ) && GameManager.instance.gameEnded == false && ShopManager.instance.shopOpen == false)
        {
            Debug.Log("Attack");
            if(map.currentPlayer.team == 'B')
            {
                for (int i = 0; i < myGameManager.redTeamPawns.Length; i++)
                {
                    if (myGameManager.redTeamPawns[i].transform.GetComponent<PlayerManager>().tileX == tileX && myGameManager.redTeamPawns[i].transform.GetComponent<PlayerManager>().tileZ == tileZ)
                    {
                        if (myGameManager.redTeamPawns[i] != map.currentPlayer)
                        {
                            if (myGameManager.redTeamPawns[i].transform.GetComponent<PlayerManager>().team != map.currentPlayer.transform.GetComponent<PlayerManager>().team)
                            {
                                myGameManager.redTeamPawns[i].transform.GetComponent<PlayerManager>().TakeDamage(map.currentPlayer.transform.GetComponent<PlayerManager>().strength);
                                if(map.currentPlayer.currentPawnClass == PlayerManager.PawnClass.Archer)
                                {
                                    int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.archerAttackArcherQuotes.Length);
                                    SoundManager.instance.Play(SoundManager.instance.archerAttackArcherQuotes[randomIndex], false);
                                }
                                else if(map.currentPlayer.currentPawnClass == PlayerManager.PawnClass.SwordMaster)
                                {
                                    int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.warriorAttackWarriorQuotes.Length);
                                    SoundManager.instance.Play(SoundManager.instance.warriorAttackWarriorQuotes[randomIndex], false);
                                    SoundManager.instance.Play(SoundManager.instance.warriorHit, false);
                                }
                            }
                        }
                    }
                }
            }
            if(map.currentPlayer.team == 'R')
            {
                for (int i = 0; i < myGameManager.blueTeamPawns.Length; i++)
                {
                    if (myGameManager.blueTeamPawns[i].transform.GetComponent<PlayerManager>().tileX == tileX && myGameManager.blueTeamPawns[i].transform.GetComponent<PlayerManager>().tileZ == tileZ)
                    {
                        if (myGameManager.blueTeamPawns[i] != map.currentPlayer)
                        {
                            if (myGameManager.blueTeamPawns[i].transform.GetComponent<PlayerManager>().team != map.currentPlayer.transform.GetComponent<PlayerManager>().team)
                            {
                                myGameManager.blueTeamPawns[i].transform.GetComponent<PlayerManager>().TakeDamage(map.currentPlayer.transform.GetComponent<PlayerManager>().strength);
                                if (map.currentPlayer.currentPawnClass == PlayerManager.PawnClass.Archer)
                                {
                                    int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.archerAttackArcherQuotes.Length);
                                    SoundManager.instance.Play(SoundManager.instance.archerAttackArcherQuotes[randomIndex], false);
                                }
                                else if (map.currentPlayer.currentPawnClass == PlayerManager.PawnClass.SwordMaster)
                                {
                                    int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.warriorAttackWarriorQuotes.Length);
                                    SoundManager.instance.Play(SoundManager.instance.warriorAttackWarriorQuotes[randomIndex], false);
                                }
                            }
                        }
                    }
                }
            }
            GameManager.instance.CheckTeamStatus();
            TileMap.instance.ResetBoard();
            if(GameManager.instance.gameEnded == false)
            {
                TurnManager.instance.SkipToNextGamePhase();
            }
        }
    }

    private void OnMouseEnter()
    {
        if(inCombatRange == true)
        {
            tileColor.material.color = Color.yellow;
        }
    }

    private void OnMouseExit()
    {
        if(inCombatRange == true)
        {
            tileColor.material.color = Color.red;
        }
    }
}
