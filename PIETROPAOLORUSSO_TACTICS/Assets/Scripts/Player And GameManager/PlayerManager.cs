﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using cakeslice;
using System;

public class PlayerManager : MonoBehaviour
{

    public int tileX;
    public int tileZ;

    public char team;

    public TileMap map;
    ClickableTile myClickableTile;

    public int currentHealth;
    public int maximumHealth;
    public int strength;
    public int minimumStrength;
    public int gold;
    public int combatRange;
    public int movementRange;
    public Animator playerAnimator;
    public GameObject highlightParticleSystem;
    public GameObject model;
    public GameObject outLinePart1;
    public GameObject outLinePart2;

    public enum PawnClass
    {
        SwordMaster,
        Archer
    }

    public PawnClass currentPawnClass;

    public bool isMoving = false;
    public bool isDead = false;

    int currentStartPoint;
    int currentEndPoint;

    float startTime;
    float journeyLength;
    float fracJourney;

    public List<Node> currentPath;
    public List<Vector3> currentPositions;

    public static Action movementEnded;

    private void Awake()
    {
        currentPath = new List<Node>();
        currentPositions = new List<Vector3>();
    }

    private void Start()
    {
        playerAnimator = GetComponentInChildren<Animator>();
        if(currentPawnClass == PawnClass.SwordMaster)
        {
            maximumHealth = 300;
            currentHealth = maximumHealth;
            strength = 150;
            minimumStrength = strength;
            gold = 200;
            movementRange = 6;
            combatRange = 1;
        }
        else if(currentPawnClass == PawnClass.Archer)
        {
            maximumHealth = 200;
            currentHealth = maximumHealth;
            strength = 100;
            minimumStrength = strength;
            gold = 200;
            movementRange = 4;
            combatRange = 6;
        }

        currentHealth = maximumHealth;
        currentStartPoint = 0;
        currentEndPoint = 1;
        transform.position = map.graph[tileX, tileZ].tile.GetComponent<ClickableTile>().pawnPosition;
        if(team == 'B')
        {
            model.GetComponent<SkinnedMeshRenderer>().material.color = Color.blue;
        }
        else if(team == 'R')
        {
            model.GetComponent<SkinnedMeshRenderer>().material.color = Color.red;
        }
    }

    void Update()
    {
        if(this.gameObject == GameManager.instance.currentPawn)
        {
            highlightParticleSystem.SetActive(true);
        }
        else
        {
            highlightParticleSystem.SetActive(false);
        }

        playerAnimator.SetBool("IsMoving", isMoving);
        if (isMoving == true && GameManager.instance.gameEnded == false)
        {
            if (fracJourney < 1f && currentStartPoint + 1 < currentPositions.Capacity && currentStartPoint != 0)
            {
                Vector3 currentNodePawnPosition = currentPositions[currentStartPoint];
                Vector3 nextNodePawnPosition = currentPositions[currentEndPoint];

                var lookPos = nextNodePawnPosition - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5);

                float distCovered = (Time.time - startTime) *2;
                float fracJourney = distCovered / Vector3.Distance(currentNodePawnPosition, nextNodePawnPosition);

                transform.position = Vector3.Lerp(currentNodePawnPosition, nextNodePawnPosition, fracJourney);
                if (transform.position == nextNodePawnPosition && currentStartPoint + 1 < currentPositions.Capacity)
                {
                    SetPoints();
                }
            }
            if(transform.position == currentPositions.First<Vector3>())
            {
                Vector3 currentNodePawnPosition = transform.position;
                Vector3 nextNodePawnPosition = currentPositions[currentStartPoint];

                var lookPos = nextNodePawnPosition - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5);

                float distCovered = (Time.time - startTime) *2;
                float fracJourney = distCovered / Vector3.Distance(currentNodePawnPosition, nextNodePawnPosition);

                transform.position = Vector3.Lerp(transform.position, nextNodePawnPosition, fracJourney);
                if (transform.position == nextNodePawnPosition && currentStartPoint + 1 < currentPositions.Capacity)
                {
                    SetPoints();
                }
            }
            if (transform.position == currentPositions.Last<Vector3>() )
            {
                isMoving = false;
                map.pathsAvailable = false;
                currentStartPoint = 0;
                currentEndPoint = 1;
                int lastX = (int)currentPositions.Last().x;
                tileX = lastX;
                int lastZ = (int)currentPositions.Last().z;
                tileZ = lastZ;
                currentPositions = null;
                map.ResetBoard();
                movementEnded();
            }
        }
        if(GameManager.instance.currentPlayer.team != team)
        {
            outLinePart1.GetComponent<cakeslice.Outline>().enabled = true;
            outLinePart1.GetComponent<cakeslice.Outline>().color = 0;
            outLinePart2.GetComponent<cakeslice.Outline>().enabled = true;
            outLinePart2.GetComponent<cakeslice.Outline>().color = 0;
        }
        else if(GameManager.instance.currentPlayer.team == team)
        {
            outLinePart1.GetComponent<cakeslice.Outline>().enabled = false;
            outLinePart2.GetComponent<cakeslice.Outline>().enabled = false;
        }
    }

    void SetPoints()
    {
        startTime = Time.time;
        currentStartPoint++;
        currentEndPoint++;
    }

    public void MovePawn()
    {
        if(currentPath == null)
        {
            return;
        }
        isMoving = true;
        float startTime = Time.time;
        map.pathChosen = false;
    }

    public void TakeDamage(int x)
    {
        currentHealth -= x;
        if(currentHealth <= 0)
        {
            Death();
        }
    }

    public void Heal(int x)
    {
        if(currentHealth < maximumHealth && currentHealth + x <= maximumHealth)
        {
            currentHealth += x;
        }
        else if(currentHealth >= maximumHealth || currentHealth + x > maximumHealth)
        {
            currentHealth = maximumHealth;
        }
    }

    public void IncreaseHealth(int x)
    {
        maximumHealth += x;
    }

    public void IncreaseStrength (int x)
    {
        strength += x;
    }

    public void IncreaseMovement(int x)
    {
        movementRange += x;
    }

    public void IncreaseAttackRange(int x)
    {
        combatRange += x;
    }

    public void EarnGold(int x)
    {
        gold += x;
    }

    public void ConsumeGold(int x)
    {
        if(gold > 0 && gold - x >= 0)
        {
            gold -= x;
        }
        else if(gold == 0 || gold - x < 0)
        {
            gold = 0;
        }
    }

    public void Death()
    {
        if(currentPawnClass == PawnClass.Archer)
        {
            int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.archerDeathQuotes.Length);
            SoundManager.instance.Play(SoundManager.instance.archerDeathQuotes[randomIndex], false);
        }
        else if(currentPawnClass == PawnClass.SwordMaster)
        {
            int randomIndex = UnityEngine.Random.Range(0, SoundManager.instance.warriorDeathQuotes.Length);
            SoundManager.instance.Play(SoundManager.instance.warriorDeathQuotes[randomIndex], false);
        }

        isDead = true;
        transform.position = GameManager.instance.graveyard.transform.position;
        tileX = -99;
        tileZ = -99;
        if(team == 'R')
        {
            GameManager.instance.redTeamAlive--;
        }
        else if(team == 'B')
        {
            GameManager.instance.blueTeamAlive--;
        }
    }

    public void RemoveDeathStatus()
    {
        isDead = false;
    }
}

