﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameManager : Singleton<GameManager> {

    public TileMap myMap;

    public GameObject currentPawn;
    public PlayerManager currentPlayer;

    public GameObject archerPrefab;
    public GameObject swordPrefab;

    public GameObject[] redTeamPawns;
    public GameObject[] blueTeamPawns;

    public GameObject graveyard;

    public int blueTeamCounter;
    public int redTeamCounter;
    public char currentTeam;

    public int redTeamAlive = 0;
    public int blueTeamAlive = 0;
    [HideInInspector]
    public bool gameEnded = false;
    public GameObject redTeamWins;
    public GameObject blueTeamWins;
    public event Action OnCurrentPlayerUpdate;


    private void Awake()
    {
        blueTeamCounter = 0;
        redTeamCounter = 0;

        if(StatsManager.instance.redTeamPawnsBackUp != null && StatsManager.instance.blueTeamPawnsBackUp != null)
        {
            for(int i = 0; i < redTeamPawns.Length; i++)
            {
                if(StatsManager.instance.redTeamPawnsBackUp[i].pawnClass == PlayerManager.PawnClass.Archer)
                {
                    redTeamPawns[i] = GameObject.Instantiate(archerPrefab);
                    PlayerManager redStats = redTeamPawns[i].GetComponent<PlayerManager>();
                    PlayerSave redSave = StatsManager.instance.redTeamPawnsBackUp[i];
                    redStats.gold = redSave.gold;
                    redStats.combatRange = redSave.combatRange;
                    redStats.movementRange = redSave.movementRange;
                    redStats.strength = redSave.strength;
                    redStats.maximumHealth = redSave.maximumHealth;
                    redSave.pawnClass = PlayerManager.PawnClass.Archer;
                    redStats.isDead = false;
                    redStats.currentHealth = redStats.maximumHealth;
                }
                else if(StatsManager.instance.redTeamPawnsBackUp[i].pawnClass == PlayerManager.PawnClass.SwordMaster)
                {
                    redTeamPawns[i] = GameObject.Instantiate(swordPrefab);
                    PlayerManager redStats = redTeamPawns[i].GetComponent<PlayerManager>();
                    PlayerSave redSave = StatsManager.instance.redTeamPawnsBackUp[i];
                    redStats.gold = redSave.gold;
                    redStats.combatRange = redSave.combatRange;
                    redStats.movementRange = redSave.movementRange;
                    redStats.strength = redSave.strength;
                    redStats.maximumHealth = redSave.maximumHealth;
                    redSave.pawnClass = PlayerManager.PawnClass.SwordMaster;
                    redStats.isDead = false;
                    redStats.currentHealth = redStats.maximumHealth;
                }
                redTeamAlive++;
            }
            for(int i = 0; i < blueTeamPawns.Length; i++)
            {
                if(StatsManager.instance.blueTeamPawnsBackUp[i].pawnClass == PlayerManager.PawnClass.Archer)
                {
                    blueTeamPawns[i] = GameObject.Instantiate(archerPrefab);
                    PlayerManager blueStats = blueTeamPawns[i].GetComponent<PlayerManager>();
                    PlayerSave blueSave = StatsManager.instance.blueTeamPawnsBackUp[i];
                    blueStats.gold = blueSave.gold;
                    blueStats.combatRange = blueSave.combatRange;
                    blueStats.movementRange = blueSave.movementRange;
                    blueStats.strength = blueSave.strength;
                    blueStats.maximumHealth = blueSave.maximumHealth;
                    blueStats.currentPawnClass = PlayerManager.PawnClass.Archer;
                    blueStats.isDead = false;
                    blueStats.currentHealth = blueStats.maximumHealth;
                }
                else if(StatsManager.instance.blueTeamPawnsBackUp[i].pawnClass == PlayerManager.PawnClass.SwordMaster)
                {
                    blueTeamPawns[i] = GameObject.Instantiate(swordPrefab);
                    PlayerManager blueStats = blueTeamPawns[i].GetComponent<PlayerManager>();
                    PlayerSave blueSave = StatsManager.instance.blueTeamPawnsBackUp[i];
                    blueStats.gold = blueSave.gold;
                    blueStats.combatRange = blueSave.combatRange;
                    blueStats.movementRange = blueSave.movementRange;
                    blueStats.strength = blueSave.strength;
                    blueStats.maximumHealth = blueSave.maximumHealth;
                    blueStats.currentPawnClass = PlayerManager.PawnClass.SwordMaster;
                    blueStats.isDead = false;
                    blueStats.currentHealth = blueStats.maximumHealth;
                }
                blueTeamAlive++;
            }
        }
        else
        {
            for (int i = 0; i < redTeamPawns.Length; i++)
            {
                int randomClass = UnityEngine.Random.Range(0, 101);
                if (randomClass < 60)
                {
                    redTeamPawns[i] = GameObject.Instantiate(swordPrefab);
                }
                else
                {
                    redTeamPawns[i] = GameObject.Instantiate(archerPrefab);
                }
                redTeamAlive++;
            }
            for (int i = 0; i < blueTeamPawns.Length; i++)
            {
                int randomClass = UnityEngine.Random.Range(0, 101);
                if (randomClass < 60)
                {
                    blueTeamPawns[i] = GameObject.Instantiate(swordPrefab);
                }
                else
                {
                    blueTeamPawns[i] = GameObject.Instantiate(archerPrefab);
                }
                blueTeamAlive++;
            }
        }
        
        for(int i = 0; i < redTeamPawns.Length; i++)
        {
            redTeamPawns[i].GetComponent<PlayerManager>().map = myMap;
            redTeamPawns[i].GetComponent<PlayerManager>().tileX = 0;
            redTeamPawns[i].GetComponent<PlayerManager>().tileZ = i;
            redTeamPawns[i].GetComponent<PlayerManager>().team = 'R';
            redTeamPawns[i].tag = "PlayerRed";
        }
        for(int i = 0; i < blueTeamPawns.Length; i++)
        {
            blueTeamPawns[i].GetComponent<PlayerManager>().map = myMap;
            blueTeamPawns[i].GetComponent<PlayerManager>().tileX = myMap.mapSizeX - 1;
            blueTeamPawns[i].GetComponent<PlayerManager>().tileZ = (myMap.mapSizeZ - 1) - i;
            blueTeamPawns[i].GetComponent<PlayerManager>().team = 'B';
            blueTeamPawns[i].tag = "PlayerBlue";
        }

        if(UnityEngine.Random.Range(0, 101) < 50)
        {
            currentPawn = redTeamPawns[0];
            currentPlayer = redTeamPawns[0].transform.GetComponent<PlayerManager>();
            currentTeam = 'R';
            redTeamCounter++;
        }
        else
        {
            currentPawn = blueTeamPawns[0];
            currentPlayer = blueTeamPawns[0].transform.GetComponent<PlayerManager>();
            currentTeam = 'B';
            blueTeamCounter++;
        }
    }

    // Use this for initialization
    void Start () {
        redTeamWins.SetActive(false);
        blueTeamWins.SetActive(false);
        TurnManager.instance.OnStartTurn += UpdateCurrentPlayer;
	}

    private void OnDestroy()
    {
        TurnManager.instance.OnStartTurn -= UpdateCurrentPlayer;
    }

    // Update is called once per frame
    void Update () {

        if(redTeamAlive == 0)
        {
            EndGameBlueWins();
        }
        else if(blueTeamAlive == 0)
        {
            EndGameRedWins();
        }

        //DEBUG INPUTS
        if(Input.GetKeyDown(KeyCode.B))
        {
            foreach(GameObject pawn in blueTeamPawns)
            {
                pawn.GetComponent<PlayerManager>().Death();
            }
            Debug.Log(blueTeamAlive.ToString());
        }
        
        if(Input.GetKeyDown(KeyCode.R))
        {
            foreach(GameObject pawn in redTeamPawns)
            {
                pawn.GetComponent<PlayerManager>().Death();
            }
            Debug.Log(redTeamAlive.ToString());
        }
        
        if(blueTeamAlive > 0 && redTeamAlive > 0)
        {
            gameEnded = false;
        }
        else if(blueTeamAlive <= 0 || redTeamAlive <= 0)
        {
            gameEnded = true;
        }

	}

    public void UpdateCurrentPlayer()
    {
        
        if(currentPlayer.team == 'B' && gameEnded == false)
        {
            Debug.Log("Current Player Updated - Game Manager - RED");
            for(int i = redTeamCounter; i < redTeamPawns.Length;)
            {
                if(redTeamPawns[i].GetComponent<PlayerManager>().isDead == true && gameEnded == false)
                {
                    i++;
                    if(i >= redTeamPawns.Length)
                    {
                        i = 0;
                    }
                }
                if(redTeamPawns[i].GetComponent<PlayerManager>().isDead == false)
                {
                    currentPawn = redTeamPawns[i];
                    currentPlayer = redTeamPawns[i].transform.GetComponent<PlayerManager>();
                    currentPlayer.EarnGold(200);
                    redTeamCounter = i;
                    break;
                }
            }
            if((redTeamCounter + 1) <= (redTeamPawns.Length - 1))
            {
                redTeamCounter++;
            }
            else if((redTeamCounter + 1) > (redTeamPawns.Length - 1))
            {
                redTeamCounter = 0;
            }
            OnCurrentPlayerUpdate();
            currentTeam = 'R';
        }
        else if(currentPlayer.team == 'R' && gameEnded == false)
        {
            Debug.Log("Current Player Updated - Game Manager - BLUE");
            for(int i = blueTeamCounter; i < blueTeamPawns.Length;)
            {
                if(blueTeamPawns[i].GetComponent<PlayerManager>().isDead == true && gameEnded == false)
                {
                    i++;
                    if(i >= blueTeamPawns.Length)
                    {
                        i = 0;
                    }
                }
                if(blueTeamPawns[i].GetComponent<PlayerManager>().isDead == false)
                {
                    currentPawn = blueTeamPawns[i];
                    currentPlayer = blueTeamPawns[i].transform.GetComponent<PlayerManager>();
                    currentPlayer.EarnGold(200);
                    blueTeamCounter = i;
                    break;
                }
            }
            if((blueTeamCounter + 1) <= (blueTeamPawns.Length - 1))
            {
                blueTeamCounter++;
            }
            else if((blueTeamCounter + 1) > (blueTeamPawns.Length - 1))
            {
                blueTeamCounter = 0;
            }
            OnCurrentPlayerUpdate();
            currentTeam = 'B';
        }
    }

    public void EndGameBlueWins()
    {
        Debug.Log("BlueTeamWins");
        redTeamWins.SetActive(true);
        StatsManager.instance.SaveGameStats();
    }
    public void EndGameRedWins()
    {
        Debug.Log("RedTeamWins");
        blueTeamWins.SetActive(true);
        StatsManager.instance.SaveGameStats();
    }

    public void CheckTeamStatus()
    {
        if(redTeamAlive == 0 || blueTeamAlive == 0)
        {
            gameEnded = true;
        }
        else
        {
            gameEnded = false;
        }
    }

    public void QuitGame()
    {   
        StatsManager.instance.DestroyStats();
        SceneManager.LoadScene("scn_mainMenu", LoadSceneMode.Single);
    }

    public void ContinueGame()
    {
        StatsManager.instance.SaveGameStats();
        blueTeamWins.SetActive(false);
        redTeamWins.SetActive(false);
        SceneManager.LoadScene("scn_gameLevel", LoadSceneMode.Single);
    }
}
