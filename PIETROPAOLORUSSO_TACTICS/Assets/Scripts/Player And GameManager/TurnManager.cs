﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TurnManager : Singleton<TurnManager> {

    public TileMap myMap;

    public enum Turns
    {
        updatePawnTurn,
        moveTurn,
        combatTurn,
        endTurn,
        endGame
    }

    public Turns currentTurn;

    public event Action OnStartTurn;
    public event Action OnMoveTurn;
    public event Action OnCombatTurn;

	// Use this for initialization
	void Start () {
        PlayerManager.movementEnded += SkipToNextGamePhase;
        currentTurn = Turns.updatePawnTurn;
        ExecuteGamePhase();
        StatsManager.instance.SaveGameStats();
	}

    private void OnDestroy()
    {
        PlayerManager.movementEnded -= SkipToNextGamePhase;
    }

    // Update is called once per frame
    void Update () {
	    
        
	}

    public void ExecuteGamePhase()
    {
        Debug.Log("Execute Game Phase" + currentTurn.ToString());
        switch(currentTurn)
        {
            case Turns.updatePawnTurn:
            {
                if(GameManager.instance.gameEnded == false)
                {
                    if (OnStartTurn != null)
                    {
                        Debug.Log("OnStartTurn");
                        OnStartTurn();
                    }
                }
                break;
            }
            case Turns.moveTurn:
            {
                if (GameManager.instance.gameEnded == false)
                {
                    if (OnMoveTurn != null)
                    {
                        Debug.Log("OnMoveTurn");
                        OnMoveTurn();
                    }
                }    
                break;
            }
            case Turns.combatTurn:
            {
                if (GameManager.instance.gameEnded == false)
                {
                    if (OnCombatTurn != null)
                    {
                        Debug.Log("OnCombatTurn");
                        OnCombatTurn();
                    }
                }    
                break;
            }
            case Turns.endTurn:
            {
                break;
            }
        }
    }

    public void SkipToNextGamePhase()
    {
        Debug.Log("Skip To Next Game Phase");
        switch(currentTurn)
        {
            case Turns.updatePawnTurn:
            {
                if(GameManager.instance.gameEnded == false)
                {
                    currentTurn = Turns.moveTurn;
                    ExecuteGamePhase();
                }
                break;
            }
            case Turns.moveTurn:
            {
                if(GameManager.instance.gameEnded == false)
                {
                    currentTurn = Turns.combatTurn;
                    ExecuteGamePhase();
                }
                break;
            }
            case Turns.combatTurn:
            {
                if(GameManager.instance.gameEnded == false)
                {
                    //currentTurn = Turns.endTurn;
                    currentTurn = Turns.updatePawnTurn;//TEMPORANEO, UNA VOLTA FINITO MARKET E UPGRADE DEI PLAYER, SCOMMENTA QUESTO ^
                    ExecuteGamePhase();
                }
                break;
            }
            case Turns.endTurn:
            {
                if(GameManager.instance.gameEnded == false)
                {
                    currentTurn = Turns.updatePawnTurn;
                    ExecuteGamePhase();
                }
                break;
            }
        }
    }
}
